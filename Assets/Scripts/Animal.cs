﻿using UnityEngine;
using System.Collections;

public class Animal : MonoBehaviour
{
    public bool allowSpeak;
    [HideInInspector]
    public bool speak;
    [HideInInspector]
    public int speakTime = 0;

    public AudioClip[] interactiveSound;

    public Animator[] leftAnimator;

    public Animator[] rightAnimator;

    public float animationLong = 3.0f;
    private float animationTime;

    // Use this for initialization
    void Start()
    {

    }

    public void Speak(bool right)
    {
        if (allowSpeak)
        {
            speak = true;
            //gameObject.GetComponent<AudioSource>().PlayOneShot(interactiveSound[speakTime++]);
            if (right)
            {
                rightAnimator[speakTime].gameObject.SetActive(true);

            }
            else
            {
                leftAnimator[speakTime].gameObject.SetActive(true);
            }


        }

    }
    public void StopSpeak()
    {
        speak = false;
        leftAnimator[speakTime].gameObject.SetActive(false);
        rightAnimator[speakTime].gameObject.SetActive(false);

        ++speakTime;
        if (speakTime == rightAnimator.Length)
        {
            speakTime = 0;
        }
     


      

    }
    // Update is called once per frame
    void Update()
    {

    }
}
