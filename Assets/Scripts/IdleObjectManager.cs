﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IdleObjectManager : DreamLandObjectManager
{
    public List<GameObject> idleObjects;

    [SerializeField]
    private Texture2D defaultTexture;

    [SerializeField]
    private GameObject controlUFO;


    private List<bool> resetes;

    private int currentIndex;

    private List<GameObject> addedIdleObjects;

    private bool full;
    // Use this for initialization
    void Start()
    {
        //resetes = new List<bool>();

        //for (int i = 0; i < idleObjects.Count; i++)
        //{
        //    resetes.Add(false);
        //}
        //for (int i = 0; i < idleObjects.Count; i++)
        //{
        //    GameObject gObject = idleObjects[i];
        //    gObject = Instantiate(gObject);
        //   // gObject.SetActive(false);
        //    idleObjects[i] = gObject;
        //}
        addedIdleObjects = new List<GameObject>();
    }
    [ContextMenu("Add houses")]
    private void AddDefaultHouse()
    {
        CreateIdleObject(defaultTexture);
    }

    public override void AddObject(Texture2D texture, string name = null)
    {
        CreateIdleObject(texture);
    }
    public void CreateIdleObject(Texture2D texture)
    {
        if (full)
        {

            GameObject first = addedIdleObjects[0];
            if (first.GetComponent<IdleObject>().stateId == IdleStateId.START)
            {
                return;
            }
          
            addedIdleObjects.RemoveAt(0);
            Destroy(first);
        }
        GameObject gObject = Instantiate(idleObjects[currentIndex]) as GameObject;
        //gObject.GetComponent<IdleObject>().Init();
        GameObject ufo = Instantiate(controlUFO);
        //gObject.SetActive(false);
        MeshRenderer[] meshRenderers = gObject.GetComponentsInChildren<MeshRenderer>();
        SkinnedMeshRenderer[] skinnedMeshRenderers = gObject.GetComponentsInChildren<SkinnedMeshRenderer>();
        Material newMaterial = new Material(Shader.Find("Unlit/Texture"));

        for (int j = 0; j < skinnedMeshRenderers.Length; j++)
        {
            skinnedMeshRenderers[j].material = newMaterial;
            skinnedMeshRenderers[j].material.SetTexture("_MainTex", texture);
        }
        for (int j = 0; j < meshRenderers.Length; j++)
        {
            meshRenderers[j].material = newMaterial;
            meshRenderers[j].material.SetTexture("_MainTex", texture);
        }

        gObject.GetComponent<IdleObject>().Init();
        addedIdleObjects.Add(gObject);
        currentIndex = ++currentIndex % idleObjects.Count;
        //idleObjects[currentIndex].SetActive(true);
        if (currentIndex == 0)
        {
            full = true;
        }
        //resetes[currentIndex] = true;

    }
    // Update is called once per frame
    void Update()
    {
    }


}
