﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveDirection
{
    Right,
    Left
}
public class BaseAnimalController : MonoBehaviour
{
    [HideInInspector]
    public Vector3 StartPoint;
    [HideInInspector]
    public Vector3 EndPoint;
    [SerializeField]
    private float _speed;

    [SerializeField]
    private int _numberOfInteractions;

    [SerializeField]
    private Collider2D _collider;

    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private SpriteRenderer _spriteRenderer;

    [SerializeField]
    private float timeAnimateParticle;

    private float currentTimeParticle;

    private bool stopParticle = false;

    public MoveDirection direction;

    [HideInInspector]
    public bool IsRunning;

    void Start()
    {
        if (direction == MoveDirection.Right)
        {
            transform.position = StartPoint;
            transform.right = (EndPoint - StartPoint).normalized;
        }
        else
        {
            transform.position = EndPoint;
            transform.right = (StartPoint - EndPoint).normalized;
        }
        GetComponentInChildren<ParticleSystem>().Play();
        IsRunning = true;
    }

    // Update is called once per frame
    private void Update()
    {
        if (IsRunning)
        {
            if (!stopParticle)
            {
                currentTimeParticle += Time.deltaTime;
                if (currentTimeParticle >= timeAnimateParticle)
                {
                    stopParticle = true;
                    GetComponentInChildren<ParticleSystem>().Stop();
                }
            }

            transform.position += transform.right * _speed * Time.deltaTime;

            if (transform.position.x > EndPoint.x || transform.position.x < StartPoint.x)
            {
                transform.right = -transform.right;
                //				_spriteRenderer.flipY = !_spriteRenderer.flipY;
            }

        }
    }

    private void OnTouch(Vector2 point)
    {
        if (_collider.OverlapPoint(point))
        {
            //			Debug.Log("Touch");
            var interactID = _animator.GetInteger("InteractID");
            if (interactID == 0)
            {
                _animator.SetInteger("InteractID", Random.Range(0, _numberOfInteractions) + 1);

            }
        }
    }
}
