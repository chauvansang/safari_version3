﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class VehicleScaleControl : ScaleControl
{
 
    [SerializeField]
    public float speedScale;

    private float _nextScale;
    private float progress;

    public override void ProgressScale()
    {
        //target = transform.parent.gameObject;
        progress += Time.deltaTime * speedScale;

        target.transform.localScale = Vector3.Lerp(target.transform.localScale,
             new Vector3(_nextScale, _nextScale, 1), progress);

    }

    public override void SetScale(float nextScale)
    {
        _nextScale = nextScale;
        progress = 0;

        progress += Time.deltaTime * speedScale;

        target.transform.localScale = Vector3.Lerp(target.transform.localScale,
             new Vector3(_nextScale, _nextScale, 1), progress);
    }
}