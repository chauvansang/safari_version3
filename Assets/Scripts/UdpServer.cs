﻿using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
public class UdpServer : MonoBehaviour
{
    [SerializeField]
    private int _port = 30000;
    [SerializeField]
    private SpeechReceiver speechReceiver;
    [SerializeField]
    private bool connect;
    private bool precessData = false;

    private UdpClient udpClient;

    private string message = "";

    private Thread thread;

    // Use this for initialization
    void Start()
    {
        if (connect)
        {
            thread = new Thread(new ThreadStart(ThreadMethod));
            thread.Start();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (precessData)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                speechReceiver.ReceiveVoice(message);

            });
            message = "";
            precessData = false;
            //speechReceiver.ReceiveVoice(message);

        }
    }

    private void ThreadMethod()
    {
        udpClient = new UdpClient(_port);
        while (true)
        {
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

            byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);

            message = Encoding.ASCII.GetString(receiveBytes);

            if (message == "1\n")
            {
                precessData = true;
            }
        }
    }
}
