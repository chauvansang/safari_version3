﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


[System.Serializable]
public class VehicleStartState : VehicleState
{
    private Vector3 targetPosition;

    public float speed;

    private float currentScale;

    public override void Init(ControlRoad vehicleRoad)
    {
        base.Init(vehicleRoad);
    }

    public override void StartState()
    {
        targetPosition = _vehicleRoad.getCurrentWayPointPosition();
        //do not change z coordinate because it may go behind some object
        targetTranform.position = new Vector3(targetTranform.position.x, targetTranform.position.y, targetPosition.z
            );
        scaleControl.SetScale(_vehicleRoad.getCurrentScale());
    }

    public override void UpdateState()
    {
        scaleControl.ProgressScale();
        float step = speed * Time.deltaTime;
        // Move our position a step closer to the target.
        targetTranform.position = Vector3.MoveTowards(targetTranform.position, targetPosition, step);

        if (targetTranform.position == targetPosition)
        {

            if (finishStartState != null)
            {
                finishStartState.Invoke();
            }
        }
    }

    public override void Finish()
    {

    }

    public override void Interact()
    {

    }
}