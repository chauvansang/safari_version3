﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public class VehicleMomentControl :  MomentControl
{
    //target gameObject to control

    //speed of gameObject

    //step to move with speed to target point
    private float step;
    public override void MoveToNextPoint(Vector3 nextPoint)
    {
        step = speed * Time.deltaTime;
        target.transform.position = Vector3.MoveTowards(target.transform.position, nextPoint, step);
    }


}