﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FlyInteractive : IInteractive
{
    [SerializeField]
    private float percentAccelerate;

    private float oldSpeed;

    private float newSpeed;
    
    public override void Init(GameObject target)
    {
        base.Init(target);

    }
    public override void OnInteractive()
    {
        if (!interact)
        {
            oldSpeed = _target.GetComponent<ControlRoad>().momentControl.speed;
            newSpeed = oldSpeed * percentAccelerate;
            _target.GetComponent<ControlRoad>().momentControl.speed = newSpeed;
            interact = true;
        }

    }

    public override void StopInteractive()
    {
        _target.GetComponent<ControlRoad>().momentControl.speed = oldSpeed;
        time = 0;
    }

    private void Update()
    {
        if (interact)
        {
            time += Time.deltaTime;
            if(time >= timeInteract)
            {
                interact = false;
                StopInteractive();
            }
        }
    }
}