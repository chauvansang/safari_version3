﻿using UnityEngine;
using System.Collections;


public abstract class IAppearance : MonoBehaviour
{
    public abstract void  OnAppearance();

    public abstract void Stop();
}
