﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnotherAnimalManager : MonoBehaviour
{
    public List<GameObject> animals;

    public GameObject[] prefabs;
    public float offset = 60.0f;
    private int cell;
    private float width;
    private float height;
    private float z;
    private Vector3 start;
    private int index = 0;
    public bool isCreate;


    private bool isFull = false;

    // Use this for initialization
    void Start()
    {



    }
    public void addStandAnimal(Texture2D texture2D)
    {
        CreateStandAnimal(texture2D);
    }
    public void CreateStandAnimal(Texture2D texture2D)
    {

        if (isFull)
        {
            Destroy(animals[index]);
            GameObject gObject = prefabs[index];
            if(texture2D != null)
            {
                Material newMaterial = Instantiate(Resources.Load("SpriteMaterial", typeof(Material)) as Material);
                SkinnedMeshRenderer[] skinnedMeshes = gObject.GetComponentsInChildren<SkinnedMeshRenderer>();
                for (int j = 0; j < skinnedMeshes.Length; j++)
                {
                    skinnedMeshes[j].material = newMaterial;


                    skinnedMeshes[j].sharedMaterial.SetTexture("_AnimalTex", texture2D);
                }
             
            }
            gObject = Instantiate(gObject);
            animals[index] = gObject;

        }
        else
        {
            GameObject gObject = prefabs[index];
            if (texture2D != null)
            {
                Material newMaterial = Instantiate(Resources.Load("SpriteMaterial", typeof(Material)) as Material);
                SkinnedMeshRenderer[] skinnedMeshes = gObject.GetComponentsInChildren<SkinnedMeshRenderer>();
                for (int j = 0; j < skinnedMeshes.Length; j++)
                {
                    skinnedMeshes[j].material = newMaterial;


                    skinnedMeshes[j].sharedMaterial.SetTexture("_AnimalTex", texture2D);
                }

            }
            //gObject.GetComponentInChildren<SkinnedMeshRenderer>().material.mainTexture = texture2D;

            gObject = Instantiate(gObject);
            animals.Add(gObject);
        }
        //animals[index].GetComponent<SkinnedMeshRenderer>().sprite = sprite;
        //animals[index].SetActive(true);

        index += 1;

        if (index == prefabs.Length)
        {
            index = 0;
            isFull = true;
        }

    }
    // Update is called once per frame
    void Update()
    {

    }
}
