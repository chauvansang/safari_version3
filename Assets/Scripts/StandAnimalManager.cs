﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StandAnimalManager : MonoBehaviour
{
    public List<GameObject> animals;

    public GameObject[] prefabs;
    public Vector3[] positions;
    public float offset = 60.0f;
    private int cell;
    private float width;
    private float height;
    private float z;
    private Vector3 start;
    private int index = 0;
    public bool isCreate;


    private bool isFull = false;

    // Use this for initialization
    void Start()
    {

        //for (int i = 0; i < animals.Count; i++)
        //{
        //    GameObject gObject = animals[i];
        //    gObject = Instantiate(gObject);
        //    gObject.SetActive(false);

        //    animals[i] = gObject;
        //}


    }
    public void addStandAnimal(Texture2D texture2D, int index)
    {
        CreateStandAnimal(texture2D, index);
    }
    public void CreateStandAnimal(Texture2D texture2D,int prefabIndex)
    {

        if (isFull)
        {
            Destroy(animals[index]);
            GameObject gObject = prefabs[prefabIndex];
            if(texture2D != null)
            {
                SkinnedMeshRenderer[] skinnedMeshes = gObject.GetComponentsInChildren<SkinnedMeshRenderer>();
                Material newMaterial = Instantiate(Resources.Load("SpriteMaterial", typeof(Material)) as Material);

                for (int j = 0; j < skinnedMeshes.Length; j++)
                {
                    skinnedMeshes[j].material = newMaterial;

                    skinnedMeshes[j].sharedMaterial.SetTexture("_AnimalTex", texture2D);
                }
            }

            //gObject.GetComponentsInChildren<SkinnedMeshRenderer>().material.mainTexture = texture2D;
            gObject.transform.position = positions[index];
            gObject = Instantiate(gObject);
            animals[index] = gObject;

        }
        else
        {
            GameObject gObject = prefabs[prefabIndex];
            if (texture2D != null)
            {
                SkinnedMeshRenderer[] skinnedMeshes = gObject.GetComponentsInChildren<SkinnedMeshRenderer>();
                Material newMaterial = Instantiate(Resources.Load("SpriteMaterial", typeof(Material)) as Material);

                for (int j = 0; j < skinnedMeshes.Length; j++)
                {
                    skinnedMeshes[j].material = newMaterial;

                    skinnedMeshes[j].sharedMaterial.SetTexture("_AnimalTex", texture2D);
                }
            }

            //gObject.GetComponentInChildren<SkinnedMeshRenderer>().material.mainTexture = texture2D;
            gObject.transform.position = positions[index];
            gObject = Instantiate(gObject);
            animals.Add(gObject);
        }
        //animals[index].GetComponent<SkinnedMeshRenderer>().sprite = sprite;
        //animals[index].SetActive(true);

        index += 1;

        if (index == positions.Length)
        {
            index = 0;
            isFull = true;
        }

    }
    // Update is called once per frame
    void Update()
    {

    }
}
