﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour
{
    public Animator leftAnimator;
    public Animator rightAnimator;
    public Animator currentAnimator;
    // Use this for initialization

    public bool isInteractive;
    public bool isIdle;
    void Start()
    {
        currentAnimator = rightAnimator;
    }

    // Update is called once per frame
    void Update()
    {
        if (isInteractive)
        {
            currentAnimator.SetBool("Interact", true);
        }
        else
        {
            currentAnimator.SetBool("Interact", false);
        }

        if (isIdle)
        {

        }
    }
}
