﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class Waypoint : MonoBehaviour
{
    [SerializeField]
    private Vector3 plusPosition;

    [SerializeField]
    private float z;

    public LineRenderer lineRenderer;
    public Vector2 scale;
    public bool isFlip = false;
    //public
    public Vector3 offset;
    public bool isPlaneLine;
    public bool isLeft;

    [ContextMenu("Set Z Coordinate")]
    public void SetZCoordinate()
    {
        Vector3[] points = null;
        points = new Vector3[lineRenderer.positionCount];
        lineRenderer.GetPositions(points);

        if (points.Length > 0)
        {
            for (int i = 0; i < points.Length; i++)
            {
                points[i].z = z;
            }
            lineRenderer.SetPositions(points);
        }
    }


    [ContextMenu("Change Coordinate")]
    public void ChangeCoordinate()
    {
        Vector3[] points = null;
        points = new Vector3[lineRenderer.positionCount];
        lineRenderer.GetPositions(points);

        if (points.Length > 0)
        {
            for (int i = 0; i < points.Length; i++)
            {
                points[i] += plusPosition;
            }
            lineRenderer.SetPositions(points);
        }
    }

    [ContextMenu("Reverse waypoint")]
    public void ReverseWaypoint()
    {
        Vector3[] points = null;
        points = new Vector3[lineRenderer.positionCount];
        lineRenderer.GetPositions(points);
        isFlip = !isFlip;
        if (points.Length > 0)
        {
            System.Array.Reverse(points);
            lineRenderer.SetPositions(points);
        }
    }
}