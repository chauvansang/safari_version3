﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public class InteractiveIdleState : IdleObjectState
{

    public IInteractive interactive;


    private float time;

    private bool interact;
    public override void FinishState()
    {
        interactive.StopInteractive();
    }

    public override void Interact()
    {
        interact = true;
        interactive.OnInteractive();
    }

    public override void StartState()
    {
        interactive.OnInteractive();
    }

    public override void UpdateState()
    {
        //if (interact)
        //{
        //    time += Time.deltaTime;
        //    if (time >= timeInteract)
        //    {
        //        time = 0;
        //        interact = false;
        //        if (finishState != null)
        //        {
        //            finishState.Invoke();
        //            FinishState();
        //        }
        //    }
        //}

    }
}