﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public abstract class RotateControl : MonoBehaviour
{
    [HideInInspector]
    public GameObject _target;

    public virtual void Init(GameObject target)
    {
        _target = target;
    }
    public abstract void ProgressRotate(bool flip, bool reverse);
    public abstract void ResetControl();

    public abstract void CalculateRotate(Vector3 nextPosition, bool flip, bool reverse);
}