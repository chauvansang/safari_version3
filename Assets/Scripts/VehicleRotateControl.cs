﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public class VehicleRotateControl : RotateControl
{
    [SerializeField]
    private float durationRotation;

    [SerializeField]
    private bool disabled;

    private float timeRotation;

    private Quaternion startRotation;

    private Quaternion endRotation;

    private Vector3 firstRotation;
    public override void Init(GameObject target)
    {
        base.Init(target);
        firstRotation = _target.transform.eulerAngles;
        startRotation = _target.transform.rotation;
        endRotation = startRotation;
    }

    public override void CalculateRotate(Vector3 nextPosition, bool flip, bool reverse)
    {
        if (!disabled)
        {
            Vector3 direction = !flip ? _target.transform.position - nextPosition :
                nextPosition - _target.transform.position;
            timeRotation = 0;
            direction = direction.normalized;

            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            if (flip)
                angle = -angle;

            startRotation = Quaternion.Euler(_target.transform.eulerAngles.x, 0, _target.transform.eulerAngles.z);

            endRotation = Quaternion.AngleAxis(angle, new Vector3(0, 0, 1));

            timeRotation += Time.deltaTime;

            _target.transform.rotation = Quaternion.Lerp(startRotation, endRotation, timeRotation / durationRotation);
        }

        if (reverse)
        {
            _target.transform.eulerAngles = new Vector3(0, -180, _target.transform.eulerAngles.z);
        }
        if (flip)
        {
            _target.transform.eulerAngles = new Vector3(0, -180, _target.transform.eulerAngles.z);
            if (reverse)
            {
                _target.transform.eulerAngles = new Vector3(0, 0, _target.transform.eulerAngles.z);
            }
        }
    }
    public override void ProgressRotate(bool flip, bool reverse)
    {
        if (!disabled)
        {
            if (timeRotation >= durationRotation)
            {
                timeRotation = durationRotation;
            }

            timeRotation += Time.deltaTime;
            _target.transform.rotation = Quaternion.Lerp(startRotation, endRotation, timeRotation / durationRotation);

        }

        //if waypoints is go back points
        if (flip)
        {
            _target.transform.eulerAngles = new Vector3(0, -180, _target.transform.eulerAngles.z);

        }

        if (reverse)
        {
            _target.transform.eulerAngles = new Vector3(0, 0, _target.transform.eulerAngles.z);
        }
    }

    public override void ResetControl()
    {
        timeRotation = 0;
        _target.transform.eulerAngles = firstRotation;
        startRotation = _target.transform.rotation;
        endRotation = startRotation;
    }
}