﻿using UnityEngine.EventSystems;
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class InputListener : MonoBehaviour
{
    [SerializeField]
    private string _ipAddress;
    [SerializeField]
    private int _portNumber;

    private SimpleClient _simpleClient;

    public SpeechReceiver speechReceiver;
    // Use this for initialization
    void Start()
    {
        _simpleClient = new SimpleClient();
        if (_simpleClient.ConnectResult(_ipAddress, _portNumber))
        {
            Debug.Log("Connected");
        }
        else
        {
            Debug.LogWarning("Connect failed");
        }


        _simpleClient.ServerMessage += OnServerMessage;
    }

    private void OnServerMessage(string msg)
    {
        try
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                //string[] msgs = msg.Split('|');
                speechReceiver.ReceiveVoice(msg);


            });
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    private IEnumerator HandleMessage(Vector2 coordinate)
    {
        
        yield return null;
    }
    
    private void Update()
    {
        //if (_simpleClient != null)
        //{
        //    _simpleClient.SendData("{\"cmd\":\"updating\"}");
        //}
       
    }
    


    private void OnDestroy()
    {
        if (_simpleClient != null)
        {
            _simpleClient.ServerMessage -= OnServerMessage;
            _simpleClient.SendData("{\"cmd\":\"disconnect\"}");
        }

    }
}
