﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
[System.Serializable]
public class DreamLandObject
{
    public string name;
    public GameObject gameObject;
}

[System.Serializable]
public class TextureIdentify
{
    public string name;
    public Texture2D texture2D;
}
public class VehicleManager : DreamLandObjectManager
{
    public DreamLandObject[] dreamLandObjects;

    public Vector3[] z_oder;


    private List<GameObject> vehicles;

    private Dictionary<Texture2D, string> addVehicles;

    private Dictionary<string, GameObject> dreamLandObjDict;

    public int maxNumberVehicles = 6;

    private bool isFull = false;

    private int current;

    private GameObject firstVehicle;

    [SerializeField]
    private TextureIdentify[] defaultTextureIdentifies;

    [SerializeField]
    private string defaultName;

    [SerializeField]
    private int numberAdding = 1;

    private Dictionary<string, Texture2D> defautTextureDict;
 
    //add new vehicle to add list
    [ContextMenu("add vehicle")]
    public void addVehicle()
    {
        
    
        for (int i = 0; i < numberAdding; i++)
        {
            int width = Mathf.FloorToInt(defautTextureDict[defaultName].width);
            int height = Mathf.FloorToInt(defautTextureDict[defaultName].height);
            Color[] pix = defautTextureDict[defaultName].GetPixels(0, 0, width, height);
            Texture2D texture2D = new Texture2D(defautTextureDict[defaultName].width, defautTextureDict[defaultName].height);
            texture2D.SetPixels(pix);
            texture2D.Apply();

            addVehicles.Add(texture2D, defaultName);
        }
       

    }
    public override void AddObject(Texture2D texture, string name)
    {
        if (!dreamLandObjDict.ContainsKey(name))
        {
            return;
        }
        if (name == null)
        {
            name = defaultName;
        }
        if (texture == null)
        {
            addVehicles.Add(defautTextureDict[name], name);
        }
        else
        {
            addVehicles.Add(texture, name);
        }
    }
    /// <summary>
    /// check overlap with all vehicles is run
    /// </summary>
    /// <param name="gObject"></param>
    /// <returns></returns>
    private bool Overlap()
    {
        //Bounds bounds = gObject.GetComponent<ControlRoad>().inSafeZone;
        for (int i = 0; i < vehicles.Count; i++)
        {
            if (vehicles[i].GetComponent<ControlRoad>().inSafeZone)
            {
                return true;
            }
        }
        return false;
    }
    /// <summary>
    /// add new vehicle
    /// </summary>
    /// <param name="sprite"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    private bool CreateVehicle()
    {

        GameObject gObject = null;
        //get first vehicle from add list
        Texture2D texture = addVehicles.Keys.First();
        string index = addVehicles[texture];

        //check overlap
        gObject = dreamLandObjDict[index];
        gObject = Instantiate(gObject);
        gObject.GetComponent<ControlRoad>().z_offset = z_oder[current];
        gObject.name = gObject.name + current;
        gObject.GetComponent<ControlRoad>().Init();

        SkinnedMeshRenderer[] skinnedMeshRenderers = gObject.GetComponentsInChildren<SkinnedMeshRenderer>();
        MeshRenderer[] meshRenderers = gObject.GetComponentsInChildren<MeshRenderer>();
        Material newMaterial = new Material(Shader.Find("Unlit/Texture"));

        for (int j = 0; j < skinnedMeshRenderers.Length; j++)
        {
            skinnedMeshRenderers[j].material = newMaterial;
            skinnedMeshRenderers[j].material.SetTexture("_MainTex", texture);
        }
        for (int j = 0; j < meshRenderers.Length; j++)
        {
            meshRenderers[j].material = newMaterial;
            meshRenderers[j].material.SetTexture("_MainTex", texture);
        }
        ++current;
        //add to UFO


        //add new  to vehicles and remove it from add vehicles
        vehicles.Add(gObject);
        addVehicles.Remove(texture);
        if (current == maxNumberVehicles)
        {
            current = 0;
        }
        //when vehicle manager is full we will use ReplaceVehicle() method instead of AddVehicle() method
        if (vehicles.Count == maxNumberVehicles)
        {
            isFull = true;
        }
        return true;
    }

    /// <summary>
    /// remove first vehicle and add new one (this method is used when vehicle manager is full vehicle)
    /// </summary>
    /// <param name="sprite"> sprite of vehicle</param>
    /// <param name="name"> name of vehicle </param>
    private void ReplaceAnimal()
    {

        //vehicles[0].GetComponent<ControlRoad>().stopWhenFinish = true;

        //get first vehicle from add list
        Texture2D texture = addVehicles.Keys.First();
        string index = addVehicles[texture];

        //remove first vehicle
        firstVehicle = vehicles[0];
        vehicles.RemoveAt(0);
        Destroy(firstVehicle);

        //replace old vehicle by new vehicle
        GameObject gObject = Instantiate(dreamLandObjDict[index]);
        gObject.name = "animal" + current;

        gObject.GetComponent<ControlRoad>().z_offset = z_oder[current];

        gObject.GetComponent<ControlRoad>().Init();

        SkinnedMeshRenderer[] skinnedMeshes = gObject.GetComponentsInChildren<SkinnedMeshRenderer>();

        MeshRenderer[] meshRenderers = gObject.GetComponentsInChildren<MeshRenderer>();

        Material newMaterial = new Material(Shader.Find("Unlit/Texture"));

        for (int j = 0; j < skinnedMeshes.Length; j++)
        {
            skinnedMeshes[j].material = newMaterial;
            skinnedMeshes[j].material.SetTexture("_MainTex", texture);
        }
        for (int j = 0; j < meshRenderers.Length; j++)
        {
            meshRenderers[j].material = newMaterial;
            meshRenderers[j].material.SetTexture("_MainTex", texture);
        }


        //play particle
        //gObject.GetComponentInChildren<ParticleSystem>().Play();

        //add new  to vehicles and remove it from add vehicles
        vehicles.Add(gObject);
        addVehicles.Remove(texture);
        ++current;
        if (current == maxNumberVehicles)
        {
            current = 0;
        }
    }
    void Start()
    {
        addVehicles = new Dictionary<Texture2D, string>();
        vehicles = new List<GameObject>();
        dreamLandObjDict = new Dictionary<string, GameObject>();
        defautTextureDict = new Dictionary<string, Texture2D>();
        for (int i = 0; i < dreamLandObjects.Length; i++)
        {
            dreamLandObjDict.Add(dreamLandObjects[i].name, dreamLandObjects[i].gameObject);
        }
        for (int i = 0; i < defaultTextureIdentifies.Length; i++)
        {
            defautTextureDict.Add(defaultTextureIdentifies[i].name, defaultTextureIdentifies[i].texture2D);
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = addVehicles.Count - 1; i > -1; i--)
        {
            if (isFull)
            {

                ReplaceAnimal();
            }
            else
            {
                CreateVehicle();
            }
        }
    }


}
