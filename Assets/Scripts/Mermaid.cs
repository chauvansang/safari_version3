﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Mermaid : MonoBehaviour
{
    [SerializeField]
    private List<Sprite> basicLoopSprites;
    [SerializeField]
    private List<Sprite> tailtSprites;
    [SerializeField]
    private List<Sprite> eyesSprites;
    [SerializeField]
    private SpriteAnimation spriteAnimation;
   
    public bool interact;
    private void Awake()
    {
        spriteAnimation.Sprites = basicLoopSprites;
        spriteAnimation.animate = true;
    }
    // Use this for initialization
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (interact)
        {
            interact = false;
            spriteAnimation.Sprites = eyesSprites;
            spriteAnimation.Reset();
        }
    }
}
