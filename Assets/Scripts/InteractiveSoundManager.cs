﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractiveSoundManager : MonoBehaviour
{
    public List<Animal> animals;
    // Use this for initialization

    public void RemoveAnimal()
    {
        
        animals.RemoveAt(0);
        animals[0].allowSpeak = true;
    }
    public void AddAnimal(Animal animal)
    {
        animals.Add(animal);
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
