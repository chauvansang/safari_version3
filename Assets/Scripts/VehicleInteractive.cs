﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class VehicleInteractive : IInteractive
{
    [SerializeField]
    private AudioClip interactiveSound;
    [SerializeField]
    private Animator interactiveAnimator;
    public override void OnInteractive()
    {
        if(interactiveAnimator != null)
            interactiveAnimator.SetBool("touch", true);
        if(interactiveSound != null)
        {
            GetComponent<AudioSource>().PlayOneShot(interactiveSound);
        }
        interact = true;
    }

    public override void StopInteractive()
    {
        if (interactiveAnimator != null)
            interactiveAnimator.SetBool("touch", false);

        interact = false;
        time = 0;
    }

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (interact)
        {
            time += Time.deltaTime;
            if(time >= timeInteract)
            {
                StopInteractive();
            }
        }
    }
}
