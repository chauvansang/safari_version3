﻿using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class ControlRoad : MonoBehaviour
{
    [SerializeField]
    public List<Waypoint> waypoints;
    [SerializeField]
    public MomentControl momentControl;
    [SerializeField]
    public RotateControl rotateControl;
    [SerializeField]
    public ScaleControl scaleControl;
    [HideInInspector]
    public List<Vector3[]> listPoints;

    public Waypoint startWayPoint;
    [SerializeField]
    public VehicleState startState;
    [SerializeField]
    public VehicleState momentState;

    private VehicleState currentState;
    /// <summary>
    /// use when show off
    /// </summary>
    public bool showOff;

    public bool animate = false;

    //if interactive animation with raise
    public bool isInteractive;

    #region rotation variable
    private Vector3[] startPoints;
    [HideInInspector]
    public Vector2 currentScale;
    //public Vector2
    private Vector3 currentPosition;

    //stop when finish cycle
    [HideInInspector]
    public bool stopWhenFinish = false;

    [HideInInspector]
    public int currentIndexWayPoint;

    [HideInInspector]
    public int currentIndexPosition;

    private Vector3[] currentRoadPoints;

    private Vector3 nextPosition;

    private float nextScale;

    private Vector3 currentOffset;

    #endregion
    /// <summary>
    /// to use in vehicle manager to avoid overlap between two vehicle
    /// </summary>
    [HideInInspector]
    public bool inSafeZone = false;
    [HideInInspector]
    public bool finishStart = false;

    [SerializeField]
    private bool isReverse;

    private Vector3 firstRotation;
    //private Quaternion startRotation;
    // Use this for initialization
    [HideInInspector]
    public Vector3 z_offset;

    private Vector3 ufoTarget = Vector3.negativeInfinity;
    private Vector3 startTarget = Vector3.negativeInfinity;

    [SerializeField]
    private Waypoint reverseWaypoint;

    private bool initialized;

    //public CallBack finishStartCallBack;
    public void Init()
    {
        initialized = true;
        currentState = momentState;
        scaleControl.target = gameObject;
        momentControl.target = gameObject;
        rotateControl._target = gameObject;


        momentState.Init(this);
        rotateControl.Init(gameObject);

        startState.finishStartState += StartState_finishStartState;
        firstRotation = transform.eulerAngles;
        // get all points moving
        listPoints = new List<Vector3[]>();

        Vector3[] points = null;

        for (int i = 0; i < waypoints.Count; i++)
        {
            points = new Vector3[waypoints[i].lineRenderer.positionCount];
            waypoints[i].lineRenderer.GetPositions(points);
            if (points.Length > 0)
            {
                listPoints.Add(points);
            }
        }


        ReturnStart();
        momentState.StartState();

    }
    void Start()
    {
        //currentState = startState;
        if (!initialized)
        {
            Init();
        }
    }

    [ContextMenu("Reverse waypoint")]
    public void ReverseWaypoint()
    {
        Vector3[] points = null;
        points = new Vector3[reverseWaypoint.lineRenderer.positionCount];
        reverseWaypoint.lineRenderer.GetPositions(points);
        reverseWaypoint.isFlip = !reverseWaypoint.isFlip;
        if (points.Length > 0)
        {
            System.Array.Reverse(points);
            reverseWaypoint.lineRenderer.SetPositions(points);
        }
    }
    private void StartState_finishStartState()
    {
        currentState = momentState;
        currentState.StartState();
     
    }

    /// <summary>
    /// vehicle with return start point
    /// </summary>
    private void ReturnStart()
    {
        if (animate)
        {
            inSafeZone = false;
            currentIndexPosition = 0;
            currentIndexWayPoint = 0;
            finishStart = true;
            nextScale = waypoints[0].scale.x;
            currentRoadPoints = listPoints[0];
            currentPosition = listPoints[0][0];
            currentPosition += waypoints[0].offset;
            currentPosition -= z_offset;
            transform.position = currentPosition;
            nextPosition = listPoints[0][1];
            nextPosition -= z_offset;
            nextPosition += waypoints[0].offset;

            currentScale = waypoints[0].scale;
            currentOffset = waypoints[0].offset;
            currentIndexPosition = 0;
            currentIndexWayPoint = 0;
            scaleControl.SetScale(nextScale);
            rotateControl.ResetControl();
        }
    }


    public Vector3 getUFOTargetPosition()
    {
        return ufoTarget;
    }

    public Vector3 getTargetStartPosition()
    {
        return startTarget;
    }

    public Vector3 getCurrentWayPointPosition()
    {
        Vector3 point = listPoints[currentIndexWayPoint][currentIndexPosition];
        point -= z_offset;
        point += waypoints[currentIndexWayPoint].offset;
        return point;
    }

    public float getCurrentScale()
    {
        return currentScale.x - (currentScale.x - currentScale.y) / currentRoadPoints.Length * currentIndexPosition;
    }
    public void FinishUFO()
    {
        startState.StartState();
        animate = true;
    }
    // Update is called once per frame
    void Update()
    {
        //stop animate
        if (!animate)
        {
            return;
        }
        //when people touch to the vehicle
        if (isInteractive)
        {
            currentState.Interact();
            isInteractive = false;
        }
        currentState.UpdateState();

    }
}
