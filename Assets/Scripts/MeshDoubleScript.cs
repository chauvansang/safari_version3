﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDoubleScript : MonoBehaviour
{
    [ContextMenu("x2 Mesh")]
    void DoubleMesh()
    {
        var mesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;
        var triangles = mesh.triangles;
        int trianglesLength = triangles.Length;
        var trianglesList = new List<int>(triangles);
        var uvs = mesh.uv;
        int uvsLength = uvs.Length;
        var uvsList = new List<Vector2>(uvs);
        var colors = mesh.colors;
        int colorsLength = colors.Length;
        var colorsList = new List<Color>(colors);


        for (int i = 0; i < trianglesLength; i += 3)
        {
            trianglesList.Add(triangles[i + 2]);
            trianglesList.Add(triangles[i + 1]);
            trianglesList.Add(triangles[i + 0]);
        }
        for (int i = 0; i < uvsLength; ++i)
        {
            uvsList.Add(uvs[i]);
        }
        for (int i = 0; i < colorsLength; ++i)
        {
            colorsList.Add(colors[i]);
        }
        mesh.triangles = trianglesList.ToArray();
        mesh.RecalculateNormals();
    }
    [ContextMenu("Revert x2 Mesh")]
    void RevertDoubleMesh()
    {
        var mesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;
        var triangles = mesh.triangles;
        int trianglesLength = triangles.Length;
        var trianglesList = new List<int>(triangles);
        trianglesList.RemoveRange(trianglesLength / 2, trianglesLength / 2);
        var uvs = mesh.uv;
        int uvsLength = uvs.Length;
        var uvsList = new List<Vector2>(uvs);
        uvsList.RemoveRange(uvsLength / 2, uvsLength / 2);
        var colors = mesh.colors;
        int colorsLength = colors.Length;
        var colorsList = new List<Color>(colors);
        colorsList.RemoveRange(colorsLength / 2, colorsLength / 2);
        mesh.triangles = trianglesList.ToArray();
        mesh.RecalculateNormals();
    }
}
