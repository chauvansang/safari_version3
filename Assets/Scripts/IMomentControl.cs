﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
[System.Serializable]
public abstract class MomentControl: MonoBehaviour
{
    [HideInInspector]
    public GameObject target;

    public float speed;
    public abstract void MoveToNextPoint( Vector3 nextPoint );


}