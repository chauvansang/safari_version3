﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void FinishAnimation();
[RequireComponent(typeof(SpriteRenderer))]
public class SpriteAnimation : MonoBehaviour
{


    public List<Sprite> Sprites;

    public float interval = 0.3f;

    public bool loop = true;

    public bool animate = true;

    public bool reverseAnimation = false;

    [HideInInspector]
    public bool finishAnimation;


    public int currentFrame = 0;

    private float time = 0.0f;

    private SpriteRenderer spriteRenderer;

    public event FinishAnimation finishEvent;
    
    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (animate)
        {
            if (reverseAnimation)
            {
                currentFrame = Sprites.Count - 1;
                spriteRenderer.sprite = Sprites[currentFrame];

            }
            else
            {
                currentFrame = 0;

                spriteRenderer.sprite = Sprites[0];
            }
        }
       
    }

    // Update is called once per frame
    void Update()
    {
        if (animate)
        {
            if (reverseAnimation)
            {
                finishAnimation = false;
                time += Time.deltaTime;
                if (time >= interval)
                {
                    time = time - interval;
                    currentFrame -= 1;

                   
                    if (currentFrame == -1)
                    {
                        if(finishEvent != null)
                        {
                            finishEvent();
                        }
                   
                        finishAnimation = true;
                        animate = false;
                        if (loop)
                        {
                            Reset();
                        }
                    }
                    else
                    {
                        spriteRenderer.sprite = Sprites[currentFrame];
                    }
                }
            }
            else
            {
                finishAnimation = false;
                time += Time.deltaTime;
                if (time >= interval)
                {
                    time = time - interval;
                    currentFrame += 1;
                    if (currentFrame == Sprites.Count)
                    {
                        finishAnimation = true;
                        if (finishEvent != null)
                        {
                            finishEvent();
                        }
                        animate = false;
                        if (loop)
                        {
                            Reset();
                        }
                    }
                    else
                    {
                        if(currentFrame < Sprites.Count)
                        {
                            spriteRenderer.sprite = Sprites[currentFrame];
                        }

                    }
                 
                 
                }
            }

        }
    }
    public void Reset()
    {
        time = 0;
        animate = true;
        if (reverseAnimation)
        {
            currentFrame = Sprites.Count - 1;
            spriteRenderer.sprite = Sprites[currentFrame];
         
        }
        else
        {
            currentFrame = 0;

            spriteRenderer.sprite = Sprites[0];
        }
    }

}
