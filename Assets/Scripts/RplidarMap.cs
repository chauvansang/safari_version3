﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

[RequireComponent(typeof(MeshFilter)), RequireComponent(typeof(BoxCollider2D)),RequireComponent(typeof(MeshRenderer))]
public class RplidarMap : MonoBehaviour
{

    public int lidarIndex = 1;
    public bool m_onscan = false;
    public string ip;
    public int port = 20108;
    private LidarData[] m_data;
    public Mesh m_mesh;
    public float translateY = 0.01f;
    public float translateX = 0.01f;
    public Vector3 translatePosition = Vector3.zero;
    public float angle;
    public Vector2Int gridSize;
    public GameObject cluster;
    //public InteractGrid grid;

    private List<int> m_ind;

    private MeshFilter m_meshfilter;
    private MeshRenderer meshRenderer;

    private Thread m_thread;
    [HideInInspector]
    public bool m_datachanged = false;
    public List<Vector3> m_vert;

    public List<Vector2> UVList;
    public int startIndex = 0;
    public int endIndex = 720;

    public int numberCluster;
    public int maxIterations;
    [SerializeField]
    MeshTopology meshTopology;

    public bool drawMesh = false;

    [SerializeField]
    BoxCollider2D boxRange;


    [HideInInspector]
    public List<Vector2> points;
    private List<GameObject> clusters;
    [SerializeField]
    private Camera camera;
    [SerializeField]
    private int maxCluster;

    void Start()
    {
        clusters = new List<GameObject>();
        for (int i = 0; i < maxCluster; i++)
        {
            GameObject gameObject = GameObject.Instantiate(cluster);
            gameObject.SetActive(false);
            clusters.Add(gameObject);

        }
        //ratioOffsets = new float[numberOffset];
        m_meshfilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        

        points = new List<Vector2>();
        m_data = new LidarData[720];

        m_ind = new List<int>();
        m_vert = new List<Vector3>();
        for (int i = 0; i < endIndex - startIndex - 1; i++)
        {
            //m_ind.Add(i);
            m_ind.AddRange(new int[] { 0, i + 1, i + 2 });
        }
        m_mesh = new Mesh();
        m_mesh.MarkDynamic();

        if (m_onscan)
        {
            switch (lidarIndex)
            {
                case 1:
                    RplidarBinding.OnConnectTcp(ip, port);
                    RplidarBinding.StartMotor();
                    m_onscan = RplidarBinding.StartScan();
                    Debug.Log("Lidar index 1 state is " + m_onscan);
                    break;
                case 2:
                    RplidarBinding1.OnConnectTcp(ip, port);
                    RplidarBinding1.StartMotor();
                    m_onscan = RplidarBinding1.StartScan();
                    Debug.Log("Lidar index 1 state is " + m_onscan);
                    break;
                case 3:
                    RplidarBinding2.OnConnectTcp(ip, port);
                    RplidarBinding2.StartMotor();
                    m_onscan = RplidarBinding2.StartScan();
                    Debug.Log("Lidar index 1 state is " + m_onscan);
                    break;
                //case 4:
                //    RplidarBinding3.OnConnectTcp(ip, port);
                //    RplidarBinding3.StartMotor();
                //    m_onscan = RplidarBinding3.StartScan();
                //    Debug.Log("Lidar index 1 state is " + m_onscan);
                //    break;
                //case 5:
                //    RplidarBinding4.OnConnectTcp(ip, port);
                //    RplidarBinding4.StartMotor();
                //    m_onscan = RplidarBinding4.StartScan();
                //    Debug.Log("Lidar index 1 state is " + m_onscan);
                //    break;
                //case 6:
                //    RplidarBinding5.OnConnectTcp(ip, port);
                //    RplidarBinding5.StartMotor();
                //    m_onscan = RplidarBinding5.StartScan();
                //    Debug.Log("Lidar index 1 state is " + m_onscan);
                //    break;
            }

            m_thread = new Thread(GenMesh);
            m_thread.Start();
        }
    }

    void OnDestroy()
    {
        m_thread.Abort();
        switch (lidarIndex)
        {
            case 1:
                RplidarBinding.EndScan();
                RplidarBinding.EndMotor();
                RplidarBinding.OnDisconnect();
                RplidarBinding.ReleaseDrive();
                break;
            case 2:
                RplidarBinding1.EndScan();
                RplidarBinding1.EndMotor();
                RplidarBinding1.OnDisconnect();
                RplidarBinding1.ReleaseDrive();
                break;
            case 3:
                RplidarBinding2.EndScan();
                RplidarBinding2.EndMotor();
                RplidarBinding2.OnDisconnect();
                RplidarBinding2.ReleaseDrive();
                break;
            //case 4:
            //    RplidarBinding3.EndScan();
            //    RplidarBinding3.EndMotor();
            //    RplidarBinding3.OnDisconnect();
            //    RplidarBinding3.ReleaseDrive();
            //    break;
            //case 5:
            //    RplidarBinding4.EndScan();
            //    RplidarBinding4.EndMotor();
            //    RplidarBinding4.OnDisconnect();
            //    RplidarBinding4.ReleaseDrive();
            //    break;
            //case 6:
            //    RplidarBinding5.EndScan();
            //    RplidarBinding5.EndMotor();
            //    RplidarBinding5.OnDisconnect();
            //    RplidarBinding5.ReleaseDrive();
            //    break;    
        }

        m_onscan = false;
    }

    void Update()
    {
        if (m_datachanged)
        {
            m_vert.Clear();
            UVList.Clear();
            points.Clear();

            m_vert.Add(translatePosition);
            UVList.Add(translatePosition);

            // grid.ResetInteractersTrigger();

            for (int i = startIndex; i < endIndex; i++) 
            {
                Vector3 point = Quaternion.Euler(0, 0, m_data[i].theta - angle) * Vector3.right * m_data[i].distant;

                float x = m_data[i].distant * Mathf.Sin(Mathf.Deg2Rad * (m_data[i].theta - angle)) * translateX;

                float y = m_data[i].distant * Mathf.Cos(Mathf.Deg2Rad * (m_data[i].theta - angle)) * translateY;
              
                point = new Vector3(x + translatePosition.x, y + translatePosition.y, translatePosition.z);


                m_vert.Add(point);
                UVList.Add(point);

                var screenPos = Camera.main.WorldToScreenPoint(point);

                Ray touchRay = Camera.main.ScreenPointToRay(screenPos);

                Debug.DrawRay(point, touchRay.direction, Color.red);

                RaycastHit[] hitInfos = Physics.RaycastAll(touchRay, 1000f);
                float xMin = boxRange.offset.x - boxRange.size.x / 2;
                float xMax = boxRange.offset.x + boxRange.size.x / 2;
                float yMin = boxRange.offset.y - boxRange.size.y / 2;
                float yMax = boxRange.offset.y + boxRange.size.y / 2;

              
                if (point.x > xMin && point.x < xMax && point.y > yMin && point.y < yMax)
                {


                    if (point.x != 0 && point.y != 0 && point != translatePosition)
                    {

                        foreach (var item in hitInfos)
                        {
                            Debug.Log(item.collider.tag);
                            switch (item.collider.tag)
                            {
                                case "Vehicle":
                                    item.collider.GetComponent<ControlRoad>().isInteractive = true;

                                    break;
                                case "Idle":
                                    item.collider.GetComponent<IdleObject>().isInteractive = true;
                                    break;

                            }
                            if (item.collider.CompareTag("UFO"))
                            {

                                Debug.Log("ok");
                            }
                        }
                    }
                }
            }

            meshRenderer.enabled = drawMesh;
            if (drawMesh)
            {
                m_mesh.SetVertices(m_vert);
                m_mesh.uv = UVList.ToArray();
                m_mesh.SetIndices(m_ind.ToArray(), meshTopology, 0);

                m_mesh.UploadMeshData(false);
                m_meshfilter.mesh = m_mesh;
                m_mesh.bounds = new Bounds(Vector3.zero, Vector3.one * 2000);
            }
            //float realTime = Time.realtimeSinceStartup;
           // meansResults = Kmeans.Cluster(points, numberCluster, maxIterations);
            //Debug.Log(name + ": " + (Time.realtimeSinceStartup - realTime));
            //for (int i = 0; i < meansResults.Means.Length; i++)
            //{
            //    clusters[i].SetActive(true);
            //    clusters[i].transform.position = new Vector3(meansResults.Means[i].x, meansResults.Means[i].y, 0);
            //}

            m_datachanged = false;
        }
    }

    void GenMesh()
    {
        while (true)
        {
            
            int datacount = 0;

            switch (lidarIndex)
            {
                case 1:
                    datacount = RplidarBinding.GetData(ref m_data);
                    break;
                case 2:
                    datacount = RplidarBinding1.GetData(ref m_data);
                    break;
                case 3:
                    datacount = RplidarBinding2.GetData(ref m_data);
                    break;
                //case 4:
                //    datacount = RplidarBinding3.GetData(ref m_data);
                //    break;
                //case 5:
                //    datacount = RplidarBinding4.GetData(ref m_data);
                //    break;
                //case 6:
                //    datacount = RplidarBinding5.GetData(ref m_data);
                //    break;

            }


            if (datacount == 0)
            {
                Thread.Sleep(20);
            }
            else
            {
                m_datachanged = true;
            }
            //while (!m_datachanged)
            //{

            //}
        }
    }

}
