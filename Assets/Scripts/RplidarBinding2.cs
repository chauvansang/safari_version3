﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System.IO;

using System;



public class RplidarBinding2
{

//    static RplidarBinding2()
//    {
//        var currentPath = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.Process);
//#if UNITY_EDITOR_64
//        currentPath += Path.PathSeparator + Application.dataPath + "/Plugins/x86_64/";
//#elif UNITY_EDITOR_32
//        currentPath += Path.PathSeparator + Application.dataPath+ "/Plugins/x86/";
//#endif
//        Environment.SetEnvironmentVariable("PATH", currentPath);
//    }


    [DllImport("RplidarCpp2")]
    public static extern int OnConnect(string port);
    [DllImport("RplidarCpp2")]
    public static extern int OnConnectTcp(string ip, int port);
    [DllImport("RplidarCpp2")]
    public static extern bool OnDisconnect();

    [DllImport("RplidarCpp2")]
    public static extern bool StartMotor();
    [DllImport("RplidarCpp2")]
    public static extern bool EndMotor();

    [DllImport("RplidarCpp2")]
    public static extern bool StartScan();
    [DllImport("RplidarCpp2")]
    public static extern bool EndScan();

    [DllImport("RplidarCpp2")]
    public static extern bool ReleaseDrive();

    [DllImport("RplidarCpp2")]
    public static extern int GetLDataSize();

    [DllImport("RplidarCpp2")]
    private static extern void GetLDataSampleArray(IntPtr ptr);

    [DllImport("RplidarCpp2")]
    private static extern int GrabData(IntPtr ptr);

    public static LidarData[] GetSampleData()
    {
        var d = new LidarData[2];
        var handler = GCHandle.Alloc(d, GCHandleType.Pinned);
        GetLDataSampleArray(handler.AddrOfPinnedObject());
        handler.Free();
        return d;
    }

    public static int GetData(ref LidarData[] data)
    {
        var handler = GCHandle.Alloc(data, GCHandleType.Pinned);
        int count = GrabData(handler.AddrOfPinnedObject());
        handler.Free();

        return count;
    }
}
