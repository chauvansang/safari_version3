﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public delegate void FinishState();
[System.Serializable]
public abstract class VehicleState : MonoBehaviour
{
    protected ControlRoad _vehicleRoad;

    protected Transform targetTranform;

    public FinishState finishStartState;
    [HideInInspector]
    public MomentControl momentControl;
    [HideInInspector]
    public RotateControl rotateControl;
    [HideInInspector]
    public ScaleControl scaleControl;

    public virtual void Init(ControlRoad vehicleRoad)
    {
        _vehicleRoad = vehicleRoad;
        targetTranform = vehicleRoad.transform;

        scaleControl = vehicleRoad.scaleControl;
        momentControl = vehicleRoad.momentControl;
        rotateControl = vehicleRoad.rotateControl;
    }
    public abstract void StartState();

    public abstract void UpdateState();

    public abstract void Interact();

    public abstract void Finish();
}