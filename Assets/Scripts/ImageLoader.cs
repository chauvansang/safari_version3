using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;
[System.Serializable]
public class ManagerIdentity
{
    public string name;
    public DreamLandObjectManager objectManager;
}
public class ImageLoader : MonoBehaviour
{
    [SerializeField]
    private string path;
    [SerializeField]
    private ManagerIdentity[] managerIdentify;

    private Dictionary<string, DreamLandObjectManager> managerDictiocnary;

    // Use this for initialization
    void Start()
    {
        managerDictiocnary = new Dictionary<string, DreamLandObjectManager>();
        for (int i = 0; i < managerIdentify.Length; i++)
        {
            managerDictiocnary.Add(managerIdentify[i].name, managerIdentify[i].objectManager);
        }
        StartCoroutine(LoadTexture2DFromFile());

    }
    IEnumerator LoadTexture2DFromFile()
    {
        yield return new WaitForEndOfFrame();
        while (true)
        {
            try
            {
                string[] filepaths = Directory.GetFiles(path, "*.png");

                for (int i = 0; i < filepaths.Length; i++)
                {
                    try
                    {
                        WWW www = new WWW("file://" + filepaths[i]);

                        string fullPath = filepaths[i].Split('.')[0];

                        string[] nameFile = fullPath.Split('\\');


                        string nameOject = nameFile[nameFile.Length - 1];

                        string[] componentName = nameOject.Split('_');

                        nameOject = componentName[1];

                        string nameManager = componentName[0];

                        Texture2D _Tex = new Texture2D(1, 1);

                        www.LoadImageIntoTexture(_Tex);

                        if (managerDictiocnary.ContainsKey(nameManager))
                        {
                            managerDictiocnary[nameManager].AddObject(_Tex, nameOject);
                        }



                        File.Delete(filepaths[i]);
                    }
                    catch (System.Exception)
                    {
                        File.Delete(filepaths[i]);

                    }

                }

            }
            catch (System.Exception)
            {


            }
            yield return new WaitForSeconds(2f);
        }
    }
}