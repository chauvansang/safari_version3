﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public abstract class IInteractive : MonoBehaviour
{
    [SerializeField]
    protected float timeInteract;

    protected float time;

    protected bool interact;

    protected GameObject _target;

    public virtual void Init(GameObject target)
    {
        _target = target;
    }
    public abstract void OnInteractive();
    public abstract void StopInteractive();

}