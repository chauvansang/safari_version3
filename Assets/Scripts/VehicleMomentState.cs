﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class VehicleMomentState : VehicleState
{
    [SerializeField]
    private IInteractive interactive;

    public bool animate = false;

    public bool isReverse;

    private Vector3 nextPosition;

    private float nextScale;

    private Vector2 currentScale;

    private Vector3 currentOffset;

    private Vector3[] currentRoadPoints;

    private bool inSafeZone;

    private Vector3 currentPosition;


    public override void Finish()
    {

    }

    public override void StartState()
    {
        SetNewWayPoint();
        SetNextPoint();
        animate = true;
        if (interactive != null)
            interactive.Init(_vehicleRoad.gameObject);

    }
    private void Reset()
    {
        if (animate)
        {
            inSafeZone = false;
            _vehicleRoad.currentIndexPosition = 0;
            _vehicleRoad.currentIndexWayPoint = 0;
            nextScale = _vehicleRoad.waypoints[0].scale.x;
            currentRoadPoints = _vehicleRoad.listPoints[0];
            currentPosition = _vehicleRoad.listPoints[0][0];
            currentPosition += _vehicleRoad.waypoints[0].offset;
            currentPosition -= _vehicleRoad.z_offset;
            targetTranform.position = currentPosition;
            nextPosition = _vehicleRoad.listPoints[0][1];
            nextPosition -= _vehicleRoad.z_offset;
            nextPosition += _vehicleRoad.waypoints[0].offset;

            currentScale = _vehicleRoad.waypoints[0].scale;
            currentOffset = _vehicleRoad.waypoints[0].offset;
            _vehicleRoad.currentIndexPosition = 0;
            _vehicleRoad.currentIndexWayPoint = 0;

            _vehicleRoad.scaleControl.SetScale(nextScale);
            _vehicleRoad.rotateControl.ResetControl();
        }
    }
    public override void UpdateState()
    {
        if (animate)
        {
            if (targetTranform.position == nextPosition)
            {
                #region Change Road
                if (_vehicleRoad.currentIndexPosition == currentRoadPoints.Length - 1)
                {
                    if (_vehicleRoad.currentIndexWayPoint == _vehicleRoad.listPoints.Count - 1)
                    {
                        if (_vehicleRoad.stopWhenFinish)
                        {
                            return;
                        }
                        Reset();
                        return;
                    }
                    else
                    {

                        ++_vehicleRoad.currentIndexWayPoint;
                        _vehicleRoad.currentIndexPosition = 0;
                        SetNewWayPoint();
                    }

                }
                #endregion
                SetNextPoint();
            }
            // in progress to to next point
            else
            {
                // we rotate between two points with duration make it smoothly
                _vehicleRoad.rotateControl.ProgressRotate(_vehicleRoad.waypoints[_vehicleRoad.currentIndexWayPoint].isFlip, isReverse);
                _vehicleRoad.scaleControl.ProgressScale();
                _vehicleRoad.momentControl.MoveToNextPoint(nextPosition);
            }
        }

    }

    private void SetNewWayPoint()
    {
        currentRoadPoints = _vehicleRoad.listPoints[_vehicleRoad.currentIndexWayPoint];
        currentPosition = currentRoadPoints[_vehicleRoad.currentIndexPosition];
        currentScale = _vehicleRoad.waypoints[_vehicleRoad.currentIndexWayPoint].scale;
        currentOffset = _vehicleRoad.waypoints[_vehicleRoad.currentIndexWayPoint].offset;
        currentPosition -= _vehicleRoad.z_offset;
        currentPosition += currentOffset;
        targetTranform.position = currentPosition;
    }
    private void SetNextPoint()
    {
        //go to next points in one road
        nextPosition = currentRoadPoints[++_vehicleRoad.currentIndexPosition];

        //set scale to next scale
        nextScale = currentScale.x - (currentScale.x - currentScale.y) / currentRoadPoints.Length * _vehicleRoad.currentIndexPosition;
        _vehicleRoad.scaleControl.SetScale(nextScale);

        nextPosition += currentOffset;
        nextPosition -= _vehicleRoad.z_offset;

        _vehicleRoad.momentControl.MoveToNextPoint(nextPosition);

        _vehicleRoad.rotateControl.CalculateRotate(nextPosition, _vehicleRoad.waypoints[_vehicleRoad.currentIndexWayPoint].isFlip, isReverse);

    }

    public override void Interact()
    {
        if(interactive != null)
            interactive.OnInteractive();
    }
}