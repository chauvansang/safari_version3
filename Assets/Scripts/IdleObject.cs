﻿using UnityEngine;
using System.Collections;

public enum IdleStateId
{
    START,
    INTERACT
}
public class IdleObject : MonoBehaviour
{

    private IdleObjectState currentState;

    [SerializeField]
    private IdleObjectState startHouseState;

    [SerializeField]
    private IdleObjectState interactState;

    [SerializeField]
    private Vector3 initPosition;

    public Vector3 startPosition;

    public Vector3 targetPosition;

    [HideInInspector]
    public IdleStateId stateId;

    public bool animate;


    public bool isInteractive;
    private float z_order;

    public void Awake()
    {
        
    }

    public void Init()
    {
        transform.position = initPosition;
        currentState = startHouseState;
        stateId = IdleStateId.START;
        startHouseState.Init(this);
        startHouseState.finishState += finishStartState;
        interactState.Init(this);
        //currentState.StartState();
    }
    public void StartState()
    {
        currentState.StartState();
        transform.position = new Vector3(transform.position.x, transform.position.y, targetPosition.z);
    }
    public void Interact()
    {
        currentState.Interact();
        isInteractive = false;
    }
    private void finishStartState()
    {
        currentState = interactState;
        
        stateId = IdleStateId.INTERACT;

    }

    public void FinishUFO()
    {
        currentState.StartState();
    }
    // Use this for initialization
    void Start()
    {
    
    }

    public void Reset()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (!animate)
        {
            return;
        }
        if (isInteractive)
        {
            Interact();
        }
        currentState.UpdateState();
    }

}
