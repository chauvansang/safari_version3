﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public class IdleObjectInteractive : IInteractive
{
    public Animator animator;
    bool AnimatorIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length >
               animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
    public override void OnInteractive()
    {
        if (!interact)
        {
            interact = true;
            animator.SetBool("touch", true);
        }
      
    }

    public override void StopInteractive()
    {
        animator.SetBool("touch", false);
        interact = false;
        time = 0;
    }

    private void Update()
    {
        if (interact)
        {
            time += Time.deltaTime;

            if (time >= timeInteract)
            {
                
                StopInteractive();
            }
        }
    }
}