﻿using UnityEngine;
using System.Collections;

public class StartIdleObjectState : IdleObjectState
{
    [SerializeField]
    private MomentControl momentControl;
    [SerializeField]
    private IInteractive interactive;

    public override void Init(IdleObject house)
    {
        base.Init(house);
  
        momentControl.target = house.gameObject;
        //interactive.Init(house.gameObject);
    }

    public override void StartState()
    {
        momentControl.target = this._house.gameObject;
        //this.targetTranform.position = new Vector3(this.targetTranform.position.x, this.targetTranform.position.y,
        //      this._house.startPosition.z);
        //interactive.OnInteractive();
    }
    public override void UpdateState()
    {
        momentControl.MoveToNextPoint(_house.targetPosition);
        if (targetTranform.position == _house.targetPosition)
        {
            if (finishState != null)
            {
                finishState.Invoke();
            }
        }
    }


    public override void FinishState()
    {

    }

    public override void Interact()
    {

    }
}
