﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechReceiver : MonoBehaviour
{
    [System.Serializable]
    public class SpeechSprite
    {

        public string animalName;
        public Sprite animalSprite;

    }
    public VehicleManager animalManager;
    public StandAnimalManager standAnimalManager;
    public AnotherAnimalManager butterflyManager;
    public AnotherAnimalManager monkeyManager;

    public SpeechSprite[] speechSprites;
    // Use this for initialization
    void Start()
    {

    }
    private Sprite Contains(string animalName)
    {
        for (int i = 0; i < speechSprites.Length; i++)
        {
            if(speechSprites[i].animalName == animalName)
            {
                return speechSprites[i].animalSprite;
            }
        }
        return null;
    }
    public void ReceiveVoice(string message)
    {
        //switch (message)
        //{
        //    case "butterfly":
        //        butterflyManager.addStandAnimal(null);
        //        break;
        //    case "monkey":
        //        monkeyManager.addStandAnimal(null);
        //        break;
        //    case "giraffe":
        //        standAnimalManager.addStandAnimal(null, 0);
        //        break;
        //    case "bunny":
        //        standAnimalManager.addStandAnimal(null, 2);
        //        break;
        //    case "horse":
        //        standAnimalManager.addStandAnimal(null, 1);
        //        break;
        //    case "lion":
        //        animalManager.addVehicle(null, 5);
        //        break;

        //    case "elephant":
        //        animalManager.addVehicle(null, 0);
        //        break;
        //    case "dinosaur1":
        //        animalManager.addVehicle(null, 2);
        //        break;
        //    case "dinosaur2":
        //        animalManager.addVehicle(null, 3);
        //        break;
        //    case "unicorn":
        //        animalManager.addVehicle(null, 4);
        //        break;
        //    case "hippo":
        //        animalManager.addVehicle(null, 1);
        //        break;
        //}
    }
    // Update is called once per frame
    void Update()
    {

    }
}

