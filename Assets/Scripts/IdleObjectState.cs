﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;



public abstract class IdleObjectState : MonoBehaviour
{
    protected IdleObject _house;

    protected Transform targetTranform;

    public FinishState finishState;
    public virtual void Init(IdleObject house)
    {
        _house = house;
        targetTranform = house.transform;
    }
    public abstract void UpdateState();

    public abstract void StartState();

    public abstract void FinishState();
    public abstract void Interact();
}