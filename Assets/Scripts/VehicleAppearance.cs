﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class VehicleAppearance : IAppearance
{
    ParticleSystem appearanceParticle;
    public override void OnAppearance()
    {
        appearanceParticle = GetComponent<ParticleSystem>();
        appearanceParticle.Play();
    }

    public override void Stop()
    {
        appearanceParticle.Stop();
    }

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
