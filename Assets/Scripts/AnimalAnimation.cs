﻿using UnityEngine;


public class AnimalAnimation : MonoBehaviour
{
    public int timeAnimation = 3;
    public bool isTouch;

    public float speed = 1.0f;
    public float speedScale = 1.0f;

    [SerializeField]
    private AudioClip interactiveSound;
    [SerializeField]
    private AudioClip appearanceSound;
    [SerializeField]
    private AudioClip firstInteractiveSound;
    [SerializeField]
    private AudioClip secondInteractiveSound;
    [SerializeField]
    private AudioClip thirdInteractiveSound;
    [SerializeField]
    private GameObject eyes;

    [SerializeField]
    private float timeChangeStand;

    private float currentTimeInteract;
    private float currentTimeStand;



    private bool moveToGround = false;

    private bool finish;


    private bool finishStart = false;


    //
    private Vector3 InitialScale;
    public Vector3 CenterScale;

    public Vector3 FinalScale;

    private Vector3 midleScale;

    private float progress;
    // The target marker.
    private Vector3 target;

    private Vector3 targetGround;

    public float offset;

    public float z_order;
    private bool stopScale;


    Vector3 positionCamera;

    public string nameIdleAnimation;
    public void PlayAppearanceSound()
    {
        //GetComponent<AudioSource>().PlayOneShot(appearanceSound);
    }
    // Use this for initialization
    void Start()
    {
        GetComponent<AudioSource>().PlayOneShot(appearanceSound);
        targetGround = transform.position;

        Camera mainCamera = Camera.main;
        target = mainCamera.ScreenToWorldPoint(
            new Vector3(mainCamera.pixelWidth / 2, mainCamera.pixelHeight / 2, 0));
        target.z = z_order;
        positionCamera = mainCamera.ScreenToWorldPoint(
            new Vector3(mainCamera.pixelWidth / 2, mainCamera.pixelHeight, 0));
        positionCamera.z = z_order;

        transform.position = positionCamera;
        InitialScale = transform.localScale;
    }
    private void OnEnable()
    {

        var ps = GetComponentInChildren<ParticleSystem>();
        //ps.gameObject.SetActive(true);
        ps.Play();
        //var module = ps.main;
        //module.duration = durationParticle;

    }
    public void Reset()
    {
        //GetComponent<AudioSource>().PlayOneShot(appearanceSound);
        transform.position = positionCamera;
        transform.localScale = InitialScale;
        var ps = GetComponentInChildren<ParticleSystem>();
        //ps.gameObject.SetActive(true);
        ps.Play();

        finishStart = false;
        moveToGround = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (!finishStart)
        {
            // The step size is equal to speed times frame time.
            float step = speed * Time.deltaTime;
            progress += Time.deltaTime * 0.5f;
            transform.localScale = Vector3.Lerp(InitialScale, CenterScale, progress);
            // Move our position a step closer to the target.
            transform.position = Vector3.MoveTowards(transform.position, target, step);
            if (transform.position == target)
            {
                finishStart = true;
                midleScale = transform.localScale;
                progress = 0;
            }
            return;
        }
        if (!moveToGround)
        {

            float step = speed * Time.deltaTime;
            progress += Time.deltaTime * speedScale;
            //Debug.Log("progress :" + progress);
            transform.localScale = Vector3.Lerp(midleScale, FinalScale, progress);
            transform.position = Vector3.MoveTowards(transform.position, targetGround, step);
            if (transform.position == targetGround)
            {
                moveToGround = true;
            }
            return;
        }

        if (nameIdleAnimation == "")
        {
            return;
        }
        if (isTouch)
        {

            if (eyes != null)
            {
                eyes.SetActive(true);

            }

            //if (GetComponent<Animal>().speakTime == 0)
            //{
            //    GetComponent<Animator>().speed = 0f;

            //    GetComponent<Animator>().Play(nameIdleAnimation, 0, 0);

            //}

            if (!GetComponent<Animal>().speak)
            {
                GetComponent<Animal>().Speak(false);
            }
            if (GetComponent<Animal>().speakTime != 0)
            {
                if (!GetComponent<Animator>().GetBool("Interact"))
                {
                    GetComponent<AudioSource>().PlayOneShot(interactiveSound);
                    GetComponent<Animator>().SetBool("Interact", true);
                }
            }
          
            //GetComponent<AudioSource>().PlayOneShot(interactiveSound);

            currentTimeInteract += Time.deltaTime;
            if (currentTimeInteract >= timeChangeStand)
            {
                if (eyes != null)
                    eyes.SetActive(false);
                GetComponent<Animal>().StopSpeak();
                // GetComponent<Animator>().speed = 1f;
                currentTimeInteract = 0;
                isTouch = false;
                GetComponent<Animator>().SetBool("Interact", false);
            }

        }
    }

    private void Animation()
    {

    }

}
