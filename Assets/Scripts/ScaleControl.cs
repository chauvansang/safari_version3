﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
[System.Serializable]
public abstract class ScaleControl :MonoBehaviour
{
    [HideInInspector]
    public GameObject target;
    public  abstract void ProgressScale();

    public abstract void SetScale(float nextScale);

}