﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;


[RequireComponent(typeof(SpriteAnimation))]
public class SkyController : MonoBehaviour
{

    [SerializeField]
    private SpriteAnimation[] element;


    private SpriteAnimation background;

    [SerializeField]
    private List<Sprite> dayLoop;

    [SerializeField]

    private List<Sprite> nightLoop;


    [SerializeField]
    private List<Sprite> nightToDay;


    [SerializeField]
    private float timeToChangeSky = 10.0f;

    private float timeChangeSky;

    private bool isNight = true;

    private bool isChanging;

    private bool startChanging;
    // Use this for initialization
    void Start()
    {
        background = GetComponent<SpriteAnimation>();

        background.Sprites = dayLoop;

        background.finishEvent += Background_finishEvent;

        isNight = false;
    }

    private void Background_finishEvent()
    {
        //finish effect change between day and night
        if (startChanging)
        {

            //reset time
            timeChangeSky = 0;
            startChanging = false;
            isChanging = true;

            
            background.Sprites = nightToDay;

            if (isNight)
            {
                background.reverseAnimation = false;

                for (int i = 0; i < element.Length; i++)
                {
                    element[i].reverseAnimation = true;
                    element[i].Reset();
                }
            }
            else
            {
                background.reverseAnimation = true;
               
                for (int i = 0; i < element.Length; i++)
                {
                    element[i].reverseAnimation = false;
                    element[i].Reset();
                }
            }
            background.Reset();

        }
        else if (isChanging)
        {

            for (int i = 0; i < element.Length; i++)
            {
                element[i].animate = false;
            }
            isChanging = false;
            timeChangeSky = 0;
            background.reverseAnimation = false;

            if (isNight)
            {
                
                background.Sprites = nightLoop;
            }
            else
            {
                background.Sprites = dayLoop;
            }
        }


    }

    // Update is called once per frame
    void Update()
    {
        timeChangeSky += Time.deltaTime;

        //begin effect change between day and night
        if (timeChangeSky >= timeToChangeSky && !startChanging && !isChanging)
        {
            isNight = !isNight;
            startChanging = true;
            

        }

    }
}
