﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HouseInteractive2 : IInteractive
{
    public Animator animator;

    bool AnimatorIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length >
               animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
    public override void OnInteractive()
    {
        if (!interact)
        {
            interact = true;
            animator.SetBool("touch", true);
            //animator.Play("take", -1, 0f);
        }

    }

    public override void StopInteractive()
    {
        animator.SetBool("touch", false);
    }

    private void Update()
    {
        if (interact)
        {
            time += Time.deltaTime;
            //if (!AnimatorIsPlaying())
            //{
            //    animator.Play("take", -1, 0f);
            //}
            if (time >= timeInteract)
            {
                interact = false;
                StopInteractive();
            }
        }
    }
}